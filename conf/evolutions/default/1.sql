# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table VolumeTemperature (
  VOL_ID                    bigint not null,
  TEMPERATURE               double(16) not null,
  VOLUME                    double(50) not null,
  OBJ_VERSION               integer not null,
  constraint uq_VolumeTemperature_TEMPERATURE unique (TEMPERATURE),
  constraint uq_VolumeTemperature_VOLUME unique (VOLUME),
  constraint pk_VolumeTemperature primary key (VOL_ID))
;

create sequence VolumeTemperature_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists VolumeTemperature;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists VolumeTemperature_seq;

