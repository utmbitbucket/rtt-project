class CookingModel
 constructor:  ->
             @bindObservables()
             @temperature= 20
             @volume=2000


    bindObservables: ->
        @temperature=ko.observable
        @volume=ko.observable


  $ ->
            ko.applyBindings new CookingModel
            $("#id_temp").attr "readonly",true


window.CookingModel=CookingModel