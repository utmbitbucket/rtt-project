package controllers;


import com.google.common.collect.*;


import play.api.Play;
import play.mvc.Controller;
import play.mvc.*;
import views.html.qunit.*;
import play.Logger;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 *  Test Controller for Qunit tests
 */

public class QunitTestController extends Controller{



    public static final String PUBLIC_PATH="public";
    public static final String PUBLIC_ASSET_PATH="\\assets";


    /**
     * List of the available qunit tests.
     */
    public static Result qunitTest(){
        List<String> testFiles = findQUnitTests();
        // System.out.println(Play.current().path());
        return ok(form.render(testFiles));
    }

    /**
     * Finds qunit tests in all modules and the application.
     *
     * @return List of js files that contain the QUnit tests.
     */
    public static List<String> findQUnitTests() {

        List<String> testFiles=new ArrayList<String>();
        testFiles= Constraints.constrainedList(testFiles,Constraints.notNull()); // guava for not allowing null entries in collection

        Path startPathLib = Paths.get(QunitTestController.PUBLIC_PATH.concat("\\lib"));
        Path startPathModels = Paths.get(QunitTestController.PUBLIC_PATH.concat("\\javascripts\\viewmodels"));
        Path startPathTests = Paths.get(QunitTestController.PUBLIC_PATH.concat("\\qunittest"));
        String pattern = "glob:*.js";
        //String patternCoffee =  "glob:*.coffee";
        try {
            Files.walkFileTree(startPathLib, new MyFileFindVisitor(pattern, testFiles));
            Files.walkFileTree(startPathModels, new MyFileFindVisitor(pattern, testFiles));
            Files.walkFileTree(startPathTests, new MyFileFindVisitor(pattern, testFiles));
            // Files.walkFileTree(startPathModelsCoffee, new MyFileFindVisitor(patternCoffee,testFiles));
        } catch (IOException e) {
            Logger.error(e.getMessage());
        }

        return Collections.unmodifiableList(testFiles); // final list, no more additions.
    }

    /*
       * nested static class for extracting js files (lib, model, test files from the public folder
     */
    static class MyFileFindVisitor extends SimpleFileVisitor<Path> {
        private PathMatcher matcher;
        List<String> files= new ArrayList<String>();

        /*
            * Constructor, accepts pattern (*.js, collection)
         */
        public MyFileFindVisitor(String pattern,List<String> files){
            this.files=files;
            try {
                matcher = FileSystems.getDefault().getPathMatcher(pattern);
            } catch(Throwable iae) {
                 Logger.error("Invalid pattern; did you forget to prefix \"glob:\"?(as in glob:*.java)");
            }
        }
        /*
            * Overriden method with child path and file attributes
         */
        public FileVisitResult visitFile(Path path, BasicFileAttributes fileAttributes){
            find(path);
            return FileVisitResult.CONTINUE;
        }
        /*
        *    finding the matching files in current path and child path and
        *    replacing Public path with asset path
        * */
        private void find(Path path) {
             Path name = path.getFileName();
            if(matcher.matches(name)) {
                    String actualPath =  path.toString();
                    String modPath=actualPath.replaceFirst(QunitTestController.PUBLIC_PATH,QunitTestController.PUBLIC_ASSET_PATH);
                    files.add(modPath);
                    // System.out.println("Matching file:" + path);
            }
        }
        /*
        * Overriden method for previsit with child path and file attributes
        **/
        public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes fileAttributes){
            find(path);
            return FileVisitResult.CONTINUE;
        }
    }


}
