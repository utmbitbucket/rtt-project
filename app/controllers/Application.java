package controllers;

import com.google.common.base.Preconditions;

import models.VolumeTemperature;
import play.data.Form;
import play.mvc.*;
import static play.data.Form.*;

import views.html.index;
import views.html.voltemp.*;

public class Application   extends Controller {
    final static Form<VolumeTemperature> volTempForm=form(VolumeTemperature.class);
    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static Result blank() {
        return ok(form.render(volTempForm));
    }

    public static Result submit(){
        VolumeTemperature volumeTemperature;
        Form<VolumeTemperature> filledUserForm= volTempForm.bindFromRequest();
        Preconditions.checkNotNull(filledUserForm);
        if(filledUserForm.hasErrors()) {
            return badRequest(form.render(filledUserForm));
        }else{

            volumeTemperature= filledUserForm.get();
        }
        return ok(form.render(volTempForm));

    }
}
