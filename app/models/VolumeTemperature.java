package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Vol Temp for rtt-project Application
 */


@Entity
@Table(name = "VolumeTemperature")
public class VolumeTemperature  extends Model {
    @Id
    @GeneratedValue
    @Column(name = "VOL_ID")
    private Long id = null;

    @Version
    @Column(name = "OBJ_VERSION")
    private int version = 0;

    @Column(name = "TEMPERATURE", length = 16, nullable = false, unique = true)
    private double temperature; // Unique and immutable

    @Column(name = "VOLUME", length = 50, nullable = false, unique = true)
    private double volume; // Unique and immutable

    // ********************** Getter/Setter Methods ********************** //
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    public int getVersion() { return version; }
    public void setVersion(int version) {this.version = version; }
    public double getTemperature() {return temperature;}
    public void setTemperature(double temperature) { this.temperature = temperature; }
    public double getVolume() {return volume; }
    public void setVolume(double volume) {this.volume = volume; }
}
