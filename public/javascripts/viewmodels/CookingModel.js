 var CookingModel = function(first, second)
 {
    this.volume = ko.observable(first);
    this.temperature = ko.observable(second);
 };
 
 ko.applyBindings(new CookingModel(1000,20)); // This makes Knockout get to work