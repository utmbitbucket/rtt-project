  	// Let's test this function  
 function isEven(val)
 {  
        return val % 2 === 0;  
 }  
         
 test('Given number is Even or Not', function()
 { 
        ok(isEven(0), 'Zero is an even number'); 
        ok(isEven(4), 'Four is Even number'); 
        ok(isEven(-4), 'So is negative four'); 
        ok(!isEven(2568/9), '2568/9 is not an even number'); 
        ok(!isEven(-7), '-7 is negative seven and not a even');  
		// Fails 
		ok(isEven(3*9), '3*9 is an even number'); 
		
 })  
	 