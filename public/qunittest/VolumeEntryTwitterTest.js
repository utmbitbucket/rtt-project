
test('Volume and Temperature test', function()
{
    var cModel = new Array();
    cModel[0] = new CookingModel(10,20);
    cModel[1] = new CookingModel(1000,20);
    cModel[2] = new CookingModel(100,30);
    cModel[3] = new CookingModel(1000,20);
    for (var i=0;i<cModel.length;i++)
    {

        equal(cModel[i].volume(),1000,"Here Volume  ..." +cModel[i].volume()+".....but expected volume is 1000");
        equal(cModel[i].temperature(),20,"Here Temperature..." +cModel[i].temperature()+".....but expected temperature is 20 is correct....");
    }
}   );
