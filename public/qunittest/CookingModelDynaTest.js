module("testing Module", {
    setup: function() {
    this.cookingModel=new CookingModel()//localStorage.getItem("cookingModelKey");
    console.log(this.cookingModel.volume);
    }, teardown: function() {
        delete this.cookingModel;
    }
});

test( "CookingModel default Values test", function() {
  equal(this.cookingModel.temperature, 20, "Default Temperature Must be 20");
  equal(this.cookingModel.volume, 2000, "Default volume Must be 2000")
});