package fluentpage;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.WebDriver;

import static org.fluentlenium.core.filter.FilterConstructor.withId;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

/**
 * Index Page TEsting for formvalidation App
 *
 */
public class VolumeTemperaturePage extends FluentPage {
    private String url;

    public VolumeTemperaturePage(WebDriver webDriver, int port) {
        super(webDriver);
        this.url = "http://localhost:" + port+"/voltemp";
    }

    @Override
    public String getUrl() {
        return this.url;
    }

    @Override
    public void isAt() {
        assert(title().equals("Volume Temperature Form"));
    }

    public void submitForm(String volume, String temperature) {
        // Fill the input field with id "name" with the passed name string.
        fill("#id_temp").with(volume);
        fill("#id_temp").with(temperature);
        //Submit the form whose id is "submit"
        submit("#id_submit");
    }
}
